package xyz.chaobei.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Copyright (C), 2015-2020
 * FileName: AdminApplication
 * Author:   MRC
 * Date:     2020/9/24 13:30
 * Description: Admin 服务启动模块
 * History:
 */
@SpringBootApplication
public class AdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class,args);
    }

}
