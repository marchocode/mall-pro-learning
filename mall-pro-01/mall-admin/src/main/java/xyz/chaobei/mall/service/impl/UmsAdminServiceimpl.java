package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.chaobei.mall.mapper.UmsAdminMapper;
import xyz.chaobei.mall.model.UmsAdminModel;
import xyz.chaobei.mall.pojo.UmsAdminPageAO;
import xyz.chaobei.mall.pojo.UmsAdminSaveAO;
import xyz.chaobei.mall.service.UmsAdminService;

import java.util.List;

/**
 * 后台用户 Service impl
 *
 * @author mrc
 * @since 2020-10-11 20:39:11
 */
@Service
public class UmsAdminServiceimpl implements UmsAdminService {

    @Autowired
    private UmsAdminMapper umsAdminMapper;

    @Override
    public List<UmsAdminModel> findAll() {
        return umsAdminMapper.selectList(null);
    }

    @Override
    public Page<UmsAdminModel> findPage(UmsAdminPageAO pageAO) {

        Page page = new Page(pageAO.getCurrent(),pageAO.getSize());
        QueryWrapper wrapper = new QueryWrapper();


        umsAdminMapper.selectPage(page, wrapper);

        return page;
    }

    @Override
    public boolean save(UmsAdminSaveAO params) {

        UmsAdminModel model = new UmsAdminModel();
        BeanUtils.copyProperties(params,model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsAdminMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsAdminSaveAO params, Integer id) {

        UmsAdminModel model = new UmsAdminModel();
        BeanUtils.copyProperties(params,model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsAdminMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsAdminMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

}
