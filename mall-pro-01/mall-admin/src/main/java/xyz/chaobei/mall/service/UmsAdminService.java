package xyz.chaobei.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import xyz.chaobei.mall.model.UmsAdminModel;
import xyz.chaobei.mall.pojo.UmsAdminSaveAO;
import xyz.chaobei.mall.pojo.UmsAdminPageAO;

import java.util.List;

/**
 * 后台用户 Service
 *
 * @author mrc
 * @since 2020-10-11 20:39:11
 */
public interface UmsAdminService {

    /**
     * <p>查询所有后台用户
     * <p>author: mrc
     *
     * @return java.util.List<xyz.chaobei.mall.model.UmsAdminModel>
     * @since 2020-10-11 20:39:11
     **/
    List<UmsAdminModel> findAll();

    /**
     * <p>默认分页请求后台用户
     * <p>author: mrc
     *
     * @param pageAO 分页入参查询参数
     * @since 2020-10-11 20:39:11
     * @return xyz.chaobei.mall.model.UmsAdminModel
     **/
    Page<UmsAdminModel> findPage(UmsAdminPageAO pageAO);

    /**
     * <p>保存一个后台用户
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @since 2020-10-11 20:39:11
     * @return boolean
     **/
    boolean save(UmsAdminSaveAO params);

    /**
     * <p>修改一个后台用户
     * <p>author: mrc
     *
     * @param id 被修改的ID 信息
     * @param params 被修改的信息
     * @since 2020-10-11 20:39:11
     * @return boolean
     **/
    boolean updateById(UmsAdminSaveAO params, Integer id);

    /**
     * <p>删除一个后台用户
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @since 2020-10-11 20:39:11
     * @return boolean
     **/
    boolean deleteById(Integer id);
}
