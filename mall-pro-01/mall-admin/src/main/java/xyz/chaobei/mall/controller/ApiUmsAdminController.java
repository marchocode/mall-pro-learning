package xyz.chaobei.mall.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import xyz.chaobei.common.api.CommonResult;
import xyz.chaobei.mall.model.UmsAdminModel;
import xyz.chaobei.mall.pojo.UmsAdminSaveAO;
import xyz.chaobei.mall.pojo.UmsAdminPageAO;
import xyz.chaobei.mall.service.UmsAdminService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * 后台用户请求控制层
 *
 * @author mrc
 * @since 2020-10-11 20:39:11
 */
@Api(tags = "ApiUmsAdminController",description = "后台用户")
@RestController
@RequestMapping("/umsAdmin")
@Validated
public class ApiUmsAdminController {

    @Autowired
    private UmsAdminService umsAdminService;


    /**
     * <p>查询所有后台用户
     * <p>author: mrc
     *
     * @return xyz.chaobei.common.api.CommonResult
     * @since 2020-10-11 20:39:11
     **/
    @ApiOperation("查询所有后台用户")
    @GetMapping("/")
    public CommonResult getAll() {

        List<UmsAdminModel> allList = umsAdminService.findAll();
        return CommonResult.success(allList);
    }

    /**
     * <p>默认分页请求后台用户
     * <p>author: mrc
     *
     * @param pageAO 分页入参查询参数
     * @since 2020-10-11 20:39:11
     * @return xyz.chaobei.common.api.CommonResult
     **/
    @ApiOperation("默认分页请求后台用户")
    @PostMapping("/page")
    public CommonResult paging(@RequestBody @ApiParam("分页入参查询参数") UmsAdminPageAO pageAO) {

        Page<UmsAdminModel> allList = umsAdminService.findPage(pageAO);
        return CommonResult.success(allList);
    }

    /**
     * <p>保存一个后台用户
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @since 2020-10-11 20:39:11
     * @return xyz.chaobei.common.api.CommonResult
     **/
    @ApiOperation("保存一个后台用户")
    @PostMapping("/")
    public CommonResult save(@RequestBody @Valid @ApiParam("保存入参信息") UmsAdminSaveAO params) {

        boolean isSave = umsAdminService.save(params);
        return CommonResult.result(isSave);
    }


    /**
     * <p>修改一个后台用户
     * <p>author: mrc
     *
     * @param id 被修改的ID 信息
     * @param params 被修改的信息
     * @since 2020-10-11 20:39:11
     * @return xyz.chaobei.common.api.CommonResult
     **/
    @ApiOperation("修改一个后台用户")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable("id") @ApiParam("被修改的ID") Integer id, @Valid @RequestBody @ApiParam("被修改的信息") UmsAdminSaveAO params) {

        boolean isUpdate = umsAdminService.updateById(params,id);
        return CommonResult.result(isUpdate);
    }

    /**
     * <p>删除一个后台用户
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @since 2020-10-11 20:39:11
     * @return xyz.chaobei.common.api.CommonResult
     **/
    @DeleteMapping("/{id}")
    @ApiOperation("删除一个后台用户")
    public CommonResult delete(@Valid @NotNull @PathVariable("id") @ApiParam("被删除的ID") Integer id) {

        boolean isDelete = umsAdminService.deleteById(id);
        return CommonResult.result(isDelete);
    }

}
