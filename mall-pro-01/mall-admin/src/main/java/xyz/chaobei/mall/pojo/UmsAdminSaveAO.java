package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 后台用户 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2020-10-11 20:39:11
 */
@Getter
@Setter
public class UmsAdminSaveAO {
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String password;

    /**
     * 头像
     */
    @ApiModelProperty("密码")
    private String icon;

    /**
     * 0锁定1正常使用
     */
    @ApiModelProperty("0锁定1正常使用")
    private Integer lock;

    /**
     * 电子邮箱
     */
    @ApiModelProperty("电子邮箱")
    private String email;

    /**
     * 昵称
     */
    @ApiModelProperty("昵称")
    private String nickName;

    /**
     * 备注信息
     */
    @ApiModelProperty("备注信息")
    private String note;
}
