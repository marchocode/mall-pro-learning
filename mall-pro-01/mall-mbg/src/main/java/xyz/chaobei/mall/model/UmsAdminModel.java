package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 后台用户实体
 *
 * @author mrc
 * @since 2020-10-11 20:39:11
 */
@Data
@TableName("ums_admin")
public class UmsAdminModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 后台管理用户
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    @TableField("`username`")
    private String username;

    /**
     * 密码
     */
    @TableField("`password`")
    private String password;

    /**
     * 头像
     */
    @TableField("`icon`")
    private String icon;

    /**
     * 0锁定1正常使用
     */
    @TableField("`lock`")
    private Integer lock;

    /**
     * 电子邮箱
     */
    @TableField("`email`")
    private String email;

    /**
     * 昵称
     */
    @TableField("`nick_name`")
    private String nickName;

    /**
     * 备注信息
     */
    @TableField("`note`")
    private String note;

    /**
     * 创建时间
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 最后登录时间
     */
    @TableField("`login_time`")
    private Date loginTime;

    /**
     * 逻辑删除标记
     */
    @TableField("`status`")
    private Integer status;

}
