package xyz.chaobei.mall.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.chaobei.common.api.CommonResult;
import xyz.chaobei.mall.pojo.UmsAdminLoginParam;
import xyz.chaobei.mall.pojo.UmsAdminTokenBO;
import xyz.chaobei.mall.service.UmsAdminService;

import javax.validation.Valid;

/**
 * 登录相关的后台接口
 *
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/15
 */
@Api(tags = "AuthController", description = "登录相关的后台接口")
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UmsAdminService umsAdminService;

    /**
     * <p>
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param param 用户名密码
     * @return xyz.chaobei.common.api.CommonResult
     * @since 2020/10/15
     **/
    @ApiOperation("用户登录接口")
    @RequestMapping("login")
    public CommonResult login(@RequestBody @Valid @ApiParam("用户名密码") UmsAdminLoginParam param) {

        UmsAdminTokenBO tokenBO = umsAdminService.umsAdminLogin(param);
        return CommonResult.success(tokenBO);
    }

}
