package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.token.TokenService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import xyz.chaobei.mall.common.exceptions.Asserts;
import xyz.chaobei.mall.mapper.UmsAdminMapper;
import xyz.chaobei.mall.model.UmsAdminModel;
import xyz.chaobei.mall.pojo.*;
import xyz.chaobei.mall.security.component.DefaultTokenServer;
import xyz.chaobei.mall.security.config.JwtConfig;
import xyz.chaobei.mall.service.UmsAdminService;

import java.util.List;

/**
 * 后台用户 Service impl
 *
 * @author mrc
 * @since 2020-10-11 20:39:11
 */
@Service
public class UmsAdminServiceimpl implements UmsAdminService {

    @Autowired
    private UmsAdminMapper umsAdminMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private DefaultTokenServer defaultTokenServer;

    @Autowired
    private JwtConfig jwtConfig;

    @Override
    public List<UmsAdminModel> findAll() {
        return umsAdminMapper.selectList(null);
    }

    @Override
    public Page<UmsAdminModel> findPage(UmsAdminPageAO pageAO) {

        Page page = new Page(pageAO.getCurrent(), pageAO.getSize());
        QueryWrapper wrapper = new QueryWrapper();


        umsAdminMapper.selectPage(page, wrapper);

        return page;
    }

    @Override
    public boolean save(UmsAdminSaveAO params) {

        UmsAdminModel model = new UmsAdminModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsAdminMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsAdminSaveAO params, Integer id) {

        UmsAdminModel model = new UmsAdminModel();
        BeanUtils.copyProperties(params, model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsAdminMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsAdminMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

    @Override
    public UmsAdminTokenBO umsAdminLogin(UmsAdminLoginParam param) {

        // 通过用户名获取userDetail
        UserDetails userDetails = this.findUserDetailByUserName(param.getUsername());
        // 基本校验用户名和密码
        if (!passwordEncoder.matches(param.getPassword(), userDetails.getPassword())) {
            Asserts.fail("用户名密码错误");
        }
        // 这里暂时不开启权限，后面再修改
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null);
        // 将构建的用户信息加入spring security context 上下文
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = defaultTokenServer.generateToken(userDetails);

        return UmsAdminTokenBO.builder().token(token).tokenHeader(jwtConfig.getTokenHeader()).build();
    }

    @Override
    public UserDetails findUserDetailByUserName(String username) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("username", username);

        UmsAdminModel dbUser = umsAdminMapper.selectOne(wrapper);
        // 基本校验用户名和密码
        if (null == dbUser) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        return new UmsAdminUserDetails(dbUser);
    }

}
