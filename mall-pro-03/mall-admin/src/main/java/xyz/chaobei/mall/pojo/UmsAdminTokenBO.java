package xyz.chaobei.mall.pojo;

import lombok.Builder;
import lombok.Data;

/**
 * 登录后token 的包装业务对象
 *
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/15
 */
@Data
@Builder
public class UmsAdminTokenBO {

    private String tokenHeader;

    private String token;
}
