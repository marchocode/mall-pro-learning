package xyz.chaobei.mall.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Copyright (C), 2015-2020
 * FileName: MyBatisPlusConfig
 * Author:   MRC
 * Date:     2020/9/24 13:32
 * Description: mybatis plus 的配置
 * 开启事务支持
 * History:
 */
@Configuration
@EnableTransactionManagement
@MapperScan({"xyz.chaobei.mall.dao","xyz.chaobei.mall.mapper"})
public class MyBatisPlusConfig {

}
