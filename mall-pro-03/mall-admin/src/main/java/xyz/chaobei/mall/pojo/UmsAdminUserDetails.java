package xyz.chaobei.mall.pojo;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import xyz.chaobei.mall.common.enums.IntEnums;
import xyz.chaobei.mall.model.UmsAdminModel;

import java.util.Collection;

/**
 * 本地用户实现 spring security 用户detail
 *
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/19
 */
public class UmsAdminUserDetails implements UserDetails {

    private final UmsAdminModel adminModel;

    public UmsAdminUserDetails(UmsAdminModel adminModel) {
        this.adminModel = adminModel;
    }
    /**
     * <p>获取权限、后面会接入。
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     * @since 2020/10/22
     * @return java.util.Collection<? extends org.springframework.security.core.GrantedAuthority>
     **/
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return adminModel.getPassword();
    }

    @Override
    public String getUsername() {
        return adminModel.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 是否启用
     * @return
     */
    @Override
    public boolean isEnabled() {
        return IntEnums.UMS_ADMIN_UN_LOCK.equals(adminModel.getLock());
    }
}
