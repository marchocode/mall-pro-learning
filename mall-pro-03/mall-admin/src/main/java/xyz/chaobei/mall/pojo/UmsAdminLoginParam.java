package xyz.chaobei.mall.pojo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/15
 */
@Getter
@Setter
public class UmsAdminLoginParam {

    @NotBlank(message = "用户名不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;

}
