package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import xyz.chaobei.common.domain.BaseModel;

import lombok.Getter;
import lombok.Setter;
import java.lang.Integer;

/**
 * 后台用户AO分页查询对象
 *
 * @author mrc
 * @since 2020-10-11 21:50:50
 */
@Getter
@Setter
public class UmsAdminPageAO extends BaseModel {

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;

    /**
     * 0锁定1正常使用
     */
    @ApiModelProperty("0锁定1正常使用")
    private Integer lock;

    /**
     * 电子邮箱
     */
    @ApiModelProperty("电子邮箱")
    private String email;

}
