package xyz.chaobei.mall.security.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 忽略的请求
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/15
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "jwt.ignored")
@Component
public class IgnoreUrlsConfig {

    private List<String> urls = new ArrayList<>();
}
