package xyz.chaobei.mall.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import xyz.chaobei.mall.security.component.DefaultAccessDeniedHandler;
import xyz.chaobei.mall.security.component.DefaultAuthenticationEntryPoint;
import xyz.chaobei.mall.security.component.JwtAuthenticationTokenFilter;

/**
 * 自定义扩展 spring security
 *
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/15
 */
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private IgnoreUrlsConfig urlsConfig;

    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http.authorizeRequests();

        // 添加开放的路径
        for (String url : urlsConfig.getUrls()) {
            registry.antMatchers(url).permitAll();
        }
        // 允许跨域预请求
        registry.antMatchers(HttpMethod.OPTIONS).permitAll();

        // 所有的请求都需要身份认证
        registry.and()
                .authorizeRequests()
                .anyRequest().authenticated()
                // 关闭csrf 不使用session
                .and()
                .csrf()
                .disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                // 自定义权限拒绝
                .and()
                .exceptionHandling()
                .accessDeniedHandler(this.customerAccessDenied())
                .authenticationEntryPoint(this.customerAuthentication())
                // 添加权限拦截器和JWT拦截器
                .and()
                .addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * <p> 自定义权限拒绝
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @return org.springframework.security.web.access.AccessDeniedHandler
     * @since 2020/10/15
     **/
    @Bean
    public AccessDeniedHandler customerAccessDenied() {
        return new DefaultAccessDeniedHandler();
    }

    /**
     * <p>自定义密码校验类
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @return org.springframework.security.crypto.password.PasswordEncoder
     * @since 2020/10/22
     **/
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * <p>自定义未登录状态返回
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @return org.springframework.security.web.AuthenticationEntryPoint
     * @since 2020/10/15
     **/
    @Bean
    public AuthenticationEntryPoint customerAuthentication() {
        return new DefaultAuthenticationEntryPoint();
    }

}
