package xyz.chaobei.mall.common.exceptions;

import lombok.Getter;
import xyz.chaobei.common.api.IErrorCode;

/**
 * 自定义异常处理类
 *
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/15
 */
@Getter
public class ApiException extends RuntimeException {

    // 异常code
    private IErrorCode errorCode;

    public ApiException(IErrorCode iErrorCode) {
        super(iErrorCode.getMessage());
        this.errorCode = iErrorCode;
    }

    public ApiException(String message) {
        super(message);
    }
}
