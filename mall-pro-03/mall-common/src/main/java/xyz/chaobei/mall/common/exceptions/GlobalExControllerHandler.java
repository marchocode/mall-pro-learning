package xyz.chaobei.mall.common.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import xyz.chaobei.common.api.CommonResult;

import java.util.Objects;

@RestControllerAdvice
@Slf4j
public class GlobalExControllerHandler {

    /**
     * <p>全局异常拦截器，拦截自定义ApiException
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param e 自定义异常
     * @return xyz.chaobei.common.api.CommonResult
     * @since 2020/10/20
     **/
    @ExceptionHandler(value = ApiException.class)
    public CommonResult exceptionHandler(ApiException e) {
        log.info("系统异常拦截器：异常信息：" + e.getMessage());
        if (Objects.nonNull(e.getErrorCode())) {
            return CommonResult.failed(e.getErrorCode());
        }
        return CommonResult.failed(e.getMessage());
    }
}