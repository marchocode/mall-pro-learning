package xyz.chaobei.mall.common.enums;

/**
 * 常用Integer 类型枚举类，通过具体的值和描述构造一个枚举
 *
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/19
 */
public enum IntEnums {

    UMS_ADMIN_LOCK(0, "用户账号锁定"),
    UMS_ADMIN_UN_LOCK(1, "用户状态正常");

    private final Integer val;

    private final String desc;

    IntEnums(Integer val, String desc) {
        this.val = val;
        this.desc = desc;
    }
}
