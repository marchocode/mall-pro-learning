## 【手摸手，带你搭建前后端分离商城系统】02 VUE-CLI 脚手架生成基本项目，axios配置请求、解决跨域问题。

回顾一下上一节我们学习到的内容。已经将一个 `usm_admin 后台用户` 表的基本增删改查全部都完成了。并且通过`swagger` 测试了我们的接口信息，并且顺利通过测试。本节将通过VUE 脚手架生成一个`vue-element ui` 的基本项目。并且完成登录页面的开发和登录逻辑的整合等等。

### 知识储备

- 使用 `vue-cli` 生成一个基本的 `VUE` 项目
- `vue-router` vue 官方路由组件
- 整合 `element ui` 
- 使用 `asiox`  封装一个发起请求的 `http.js`
- 解决开发时期的 `跨域问题` 

> vue cli:https://cli.vuejs.org/zh/
>
> vue router: https://router.vuejs.org/zh/
>
> asiox:http://axios-js.com/
>
> element ui:https://element.eleme.io/

### 前端项目生成

当然，首先要保证你的计算机安装了 `node js` ,如果顺利安装了 `node js` ，可以命令行测试如下：

```sh
D:\Project>node -v
v12.16.1
```

安装 `vue-cli`

```sh
npm install -g vue-cli
```

#### 生成 webpack 项目

使用命令初始化一个空项目，当然，这就要求你填写一些基本信息来初始化。

`vue init <template-name> <project-name>` 

- <template-name> 默认打包方式，一般就是`webpack`

```sh
D:\Project>vue init webpack mall-pro-admin
? Project name mall-pro-admin
? Project description mall-pro-admin
? Author MRC <1763907575@qq.com>
? Vue build standalone
? Install vue-router? Yes
? Use ESLint to lint your code? Yes
? Pick an ESLint preset Standard
? Set up unit tests No
? Setup e2e tests with Nightwatch? No
? Should we run `npm install` for you after the project has been created? (recommended) npm

   vue-cli · Generated "mall-pro-admin".


# Installing project dependencies ...
```

- Vue build:  `standalone` 独立构建，直接回车就行了。
- `vue-router` vue 路由是必须要安装的。
- `ESLint` 一种规范代码的方式，不嫌麻烦就yes。
- `unit tests` 单元测试，我们一般不会用到。no
- 选择 `npm` 作为我们基本的包管理。

目录结构如下：我们只关注的几个点有：

- `src 目录` 打包源码路径
- `config 目录` 配置路径

其他的等到具体的问题，我会再提起的。

```
mall-pro-admin
├── build
│   ├── build.js
│   ├── check-versions.js
│   ├── logo.png
│   ├── utils.js
│   ├── vue-loader.conf.js
│   ├── webpack.base.conf.js
│   ├── webpack.dev.conf.js
│   └── webpack.prod.conf.js
├── config
│   ├── dev.env.js
│   ├── index.js
│   └── prod.env.js
├── index.html
├── package-lock.json
├── package.json
├── README.md
├── src
│   ├── App.vue
│   ├── assets
│   │   └── logo.png
│   ├── components
│   │   └── HelloWorld.vue
│   ├── main.js
│   └── router
│       └── index.js
└── static
```

#### 试试让你的项目跑起来

```sh
cd mall-pro-admin

# 安装依赖
npm install

# 通过开发热部署方式打包运行
npm run dev
```

访问： [http://localhost:8080](http://localhost:8080)

![image-20201014112701015](https://file.chaobei.xyz/image-20201014112701015.png_imagess)

### 整合Element UI

`Element UI` 其实用起来简单，并且文档完善。有很多后台化的组件，很适合后台的开发。对于新手也很友好，所以我们就选择这个框架

> 请参考：https://element.eleme.io/#/zh-CN/component/installation

在项目根路径执行》安装依赖

```sh
npm i element-ui -S
```

#### 使用部分引入的方式

> 部分引用，可以减少我们项目的体积。并且这种方式对于熟悉组件有一定的帮助。不建议无脑全部引用。

安装 `babel-plugin-component` 

```sh
npm install babel-plugin-component -D
```

修改根目录 ` .babelrc ` 文件

```js
  "plugins": [
    "transform-vue-jsx",
    "transform-runtime",
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ]
  ]
```

当然你嫌麻烦的话。完全可以全部引用，这里只要参考官网就行。

#### 尝鲜一个组件

做完以上步骤、肯定要检查自己的整合是否有问题。不要急，这里就需要找一个组件进行测试。

> 查看我们 `components` 目录下的 `HelloWorld.vue` 文件，适当的修改一下。我们就选择一个按钮来进行测试。

```vue
<el-button>默认按钮</el-button>
```

**记得在script标签下引入需要的组件，不然不生效**

```javascript
import Vue from 'vue'
import {Button} from 'element-ui'

Vue.use(Button)
```

至此，基本的项目也构建完成，并且也整合了我们的 `UI` 框架。算是前端的骨架已搭建完成。 



### 前端请求工具类

我们知道，前后端分离的项目，所有的请求都是需要一个统一的`请求工具类` 来替我们完成的。以及路由的动态挂载，这些全部由前端请求

安装 axios

```sh
npm install axios
```

#### 创建一个工具类

按照官网的指示，我们配置一个类。将

> 请参考：http://axios-js.com/zh-cn/docs/

创建一个 `axios` 的实例，配置基础的请求路径，也就是我们后端服务的地址。

再加入一个拦截器，第一时间处理所有请求的 `code` , 引入`element ui message` 进行消息的提示。

我们使用的是 `ES6` 模块化的写法。不会的小伙伴可以参考：

> https://es6.ruanyifeng.com/#docs/module

```js
import axios from 'axios'
import {Message} from 'element-ui'

// 创建axios实例
const service = axios.create({
  baseURL: 'http://localhost/', // 基础baseurl
  timeout: 15000 // 超时时间
})

// 实现拦截器，处理code 非200的请求
service.interceptors.response.use(response => {
  const rest = response.data
  // code 非200 进行处理
  if (rest.code !== 200) {
    Message({
      message: rest.message,
      type: 'error',
      duration: 3 * 1000
    })
  } else {
    return response.data
  }
}, error => {
  // 处理异常信息
  console.log('error' + error)
  Message({
    message: error.message,
    type: 'error',
    duration: 3 * 1000
  })
  return Promise.reject(error)
})

export default service
```

因为我们后端返回的数据格式是这种最简单的方式返回的。所以我们当然要判断 `code` ，后面还会有登录失效`401` 等错误码，遇到再说

```json
{
    code: 200,
    data: true,
    message: "操作成功"
}
```

#### api 引用

那么，我们前端肯定会保存所有的后台接口信息，在 ` src` 建立一个 `api` 目录，所有的**接口信息务必归类保存**在每一个 `api.js` 下

因为我们在上一节中，已经通过工具生成了一个模块 `ums_admin` 的增删改查接口。我们挑选一个最简单的 `GET /` 

 ```javascript
import request from '@/utils/request'

/**
 * 获取全部用户信息
 * @author mrc
 * @since 2020-10-14
 */
export function allUmsAdmin () {
  return request({
    url: '/umsAdmin/',
    method: 'GET'
  })
}
 ```

前端接口也写好了。当然就剩下：`请求` 了。还是 使用原有的 `HelloWorld.vue` 页面。

```javascript
  import {allUmsAdmin} from '@/api/umsAdmin'

  created () {
    allUmsAdmin().then(response => {
      console.log(response)
    })
  }
```

这样写是完全没有问题的。在页面创建好后，就执行`allUmsAdmin` 方法。将得到的内容进行打印

#### @符号

```javascript
import {allUmsAdmin} from '@/api/umsAdmin'
```

可能有的朋友对这一行代码里面的 `@` 有所疑惑，为什么会这样写。当然，用 `../../` 这种绝对路径也是可以的。但是目录一多，很容易弄得你眼花。随之一种简便的方式就是：@

**@直接将路径等价为：/src** 这种相对路径就好用很多了。

> https://www.cnblogs.com/boshow/p/8758927.html



### 请求后端接口

当我们刷新页面访问的时候，会发现如下错误。

#### 什么是跨域 CORS

跨域是由于浏览器的 `同源策略` 的限制出现的问题；原因 在于浏览器限制：

浏览器发起的请求只能是：同一个域名、同一个端口下的。若端口不同，比如我们现在是从`8080端口` 发送请求到`80端口` 

自然违反浏览器的 **同源策略**

#### 跨域问题

我们发起的请求，居然报错了，打开 `F12` 后发现。出现的错误信息如下：

```
Access to XMLHttpRequest at 'http://localhost/umsAdmin/' from origin 'http://localhost:8080' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource.
```

#### 后端配置解决跨域问题

`Springboot` 对于跨域有着很好的解决方式，添加一个配置类即可。

```java
@Configuration
public class GlobalCorsConfig {

    /**
     * 允许跨域调用的过滤器
     */
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        //允许所有域名进行跨域调用
        config.addAllowedOrigin("*");
        //允许跨越发送cookie
        config.setAllowCredentials(true);
        //放行全部原始头信息
        config.addAllowedHeader("*");
        //允许所有请求方法跨域调用
        config.addAllowedMethod("*");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
```

重启项目，重新访问试试~

![image-20201014173741696](https://file.chaobei.xyz/image-20201014173741696.png_imagess)

已经可以正常请求数据了。至此，我们的前后端的通信已经成功测试通过了。

### 小结

通过本节，你已经学会如何用 `vue-cli` 来生成一个基本的项目、并且整合 常用的 `element ui` 等前端框架。

并且编写一个 `axios` 工具类来处理所有发送到后端的请求。

通过注解的方式解决跨域问题！

#### 源码

https://gitee.com/mrc1999/mall-pro-learning

### 持续更新中，欢迎关注

![](https://file.chaobei.xyz/blogs/banner_1591192617234.jpg)

