import request from '@/utils/request'

/**
 * 获取全部用户信息
 * @author mrc
 * @since 2020-10-14
 */
export function allUmsAdmin () {
  return request({
    url: '/umsAdmin/',
    method: 'GET'
  })
}
