## 【手摸手，带你搭建前后端分离商城系统】04 配置权限信息

从上一小节，我们已经通过整合Spring Security ,将基本的认证进行了整合，本小结将带领大家，搭建起权限验证。

权限很容易理解：因为现在的接口只要登录后、是都可以进行访问的，这样其实是不安全的，所以，我们要在这里加一个限制。让我们的接口变得更加安全、不能让任何人随意的访问我们一些重要的接口。

### 权限设计

权限设计仍旧参考我们基本的`用户-角色-权限` 的多对多关系。我这里简单的给大家画个图理解一下表与表之间的关系。

![image-20201023183000431](https://file.chaobei.xyz/image-20201023183000431.png_imagess)

- 前后端分离的项目、所有的页面路由也需要从后端加载。动态挂载到VUE
- 前端与后端的交互，只有通过请求，权限也是。每一次的请求都要进行校验权限是否存在。若不存在则不能访问接口信息。

### 理解权限校验

权限校验的简单逻辑如下：

1. 通过权限注解加入需要访问权限的接口。
2. 用户登录后获得token.
3. 携带token 访问需要授权的接口。
4. 通过查询上下文与当前权限字符进行比对。若不存在，则无权访问
5. 若存在，则成功调用接口。

可能朋友你会对`权限字符` 有所疑惑；

#### 权限字符

简单理解，权限字符是我们系统自己定义的，比如 **user:add** ，是一个用来比对的令牌，你有这个令牌，则说明你具有这一份权限。就这么简单。

#### 开启权限前置拦截

如下所示，在我们后台管理系统关于安全配置类中，加入`@EnableGlobalMethodSecurity(prePostEnabled = true)` 即可开启权限校验。

```java
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AdminSecurityConfig extends SecurityConfig {
    xxx
}
```

#### 权限拦截注解

如下注解则表示，访问这个接口，你需要一个`umsAdmin:query` 的权限字符。

```java
@PreAuthorize("hasAuthority('umsAdmin:query')")
@ApiOperation("查询所有后台用户")
@GetMapping("/")
public CommonResult getAll() {
    xxx
}
```

### 权限比对

当然，我们已经说过了。我们用户的权限要与系统要求的权限做对比，若存在则表示可以访问，反之则无权访问。

之前我们已经介绍过了，在`UserDetails` 这个接口下，有一个`getAuthorities` 方法，这个方法就是将用户所具有的权限通过一个`Collection` 集合的方式进行返回。这里可以先测试，看一下权限系统是如何运作的。

```java
Collection<? extends GrantedAuthority> getAuthorities();
```

假设：我们登陆的用户所具有`umsAdmin:query` 这样的一个权限字符。

```java
@Override
public Collection<? extends GrantedAuthority> getAuthorities() {

    List<GrantedAuthority> authorities = new LinkedList<>();
    authorities.add(new SimpleGrantedAuthority("umsAdmin:query"));
    return authorities;
}
```

#### 尝试权限访问

现在我们有两个接口，分别需要有`umsAdmin:query` 与 `umsAdmin:add` 权限才可以分别访问，但是按照上面的配置来说，我们的用户只具有`umsAdmin:query` 查询的权限，没有添加的权限，访问第二个肯定会提示无权访问。

```java
@ApiOperation("查询所有后台用户")
@GetMapping("/")
@PreAuthorize("hasAuthority('umsAdmin:query')")
public CommonResult getAll() {
	xxx
}

@ApiOperation("保存一个后台用户")
@PostMapping("/")
@PreAuthorize("hasAuthority('umsAdmin:save')")
public CommonResult save(@RequestBody @Valid @ApiParam("保存入参信息") UmsAdminSaveAO params) {
	xxx    
}
```

#### 接口测试结果

结果显而易见，通过`@PreAuthorize` 注解，最简单的实现接口级别的权限访问。

优点：

- 简单、明了、易修改
- 精确到任何配置的位置

缺点：

- 较为麻烦、每个接口都需要配置

```json
//get /umsAdmin/
{
    "code": 200,
    "message": "操作成功",
    "data": []
}
//post /usmAdmin/
{
    "code": 403,
    "data": "不允许访问",
    "message": "没有相关权限"
}
```

还有一种自定义过滤器实现的方式，通过通配符的形式进行，这里就不介绍了。相比而言，这个更加简单。也更加容易理解。

#### 修改原有权限配置

```java
//修改前 这里暂时不开启权限，后面再修改
UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null);

// 修改后 构造Authentication 时候传入的参数，加入权限字符
UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null,userDetails.getAuthorities());
```

修改后，因为我们这里启用了权限校验、所以在构建`UsernamePasswordAuthenticationToken` 的时候，就需要将权限字符传递过去。所以我们在构造 `UserDetails` 的时候，就需要将`权限的集合` 传递过去。

```java
public UmsAdminUserDetails(UmsAdminModel adminModel, Set<String> permission) {
    this.adminModel = adminModel;
    this.permission = permission;
}
```

#### 查询数据库构造动态权限

当然，我们在设计权限的时候，全部采用多对多的设计，所以查询起来会较为麻烦、不过这也是最通用的一种方法，你也可以将`用户-角色` 的多对多关系修改为`一个用户一个角色` 这样来设计，完全没有任何关系。按照自己的业务逻辑来。这才是最重要的。

> 具体构造过程我就不细说了，还请查看源码。我都有注释的，相信你可以看懂。



### 开始前端页面搭建

写了这么多接口、还有权限。总要把它接入到具体的地方来使用吧。我们就着手搭建一个`普普通通`的登录页面，以及一个`普普通通` 的后台展示页面，看到这里，或许你该复习一下：

- VUE 基础
- VUE Router
- ES 6 语法



#### 添加路由和页面

我们主要要添加两个页面，一个作为登录，一个作为登录后的主页面。

找到我们`src/router/index.js` ，推荐使用`WEB-Storm` 开发我们的项目。创建一个VUE组件也非常的方便

创建：

- `view/Login.vue`  登录页面
- `view/Index.vue` 主页面

```json
  routes: [
    {
      path: '/',
      name: '首页',
      component: Index
    },
    {
      path: '/login',
      name: '登录页面',
      component: Login
    }
  ]
```

相比大家能懂得这里涵盖的意思，当我们访问`http:项目域名/` 就会跳转到我们的首页，而访问`/login` 则是跳转到登录页面

#### 小试牛刀

首先来尝试写一个登录页面，页面说实在的，真的没什么可说的。但是我想提一下我用到的一些组件。

- ROW 组件
- COL 组件

![image-20201101215639597](https://file.chaobei.xyz/image-20201101215639597.png_imagess)

按照最简单的方式，我们的登录页面也是，不带那些 `花里胡哨` 只求我们页面的`简单大方`

